package com.test.util;

import com.test.po.Page;

public class PageUtil {

    private static ThreadLocal<Page> PAGELOCAL = new ThreadLocal<Page>();

    public static void pageStart(Page page){
        PAGELOCAL.set(page);
    }

    public static Page getLocalPage(){
        return PAGELOCAL.get();
    }
}
