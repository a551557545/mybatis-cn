package com.test;

import com.test.mapper.RoleMapper;
import com.test.po.Page;
import com.test.po.Role;
import com.test.util.PageUtil;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        String resource = "com/test/mybatis-config.xml";
        InputStream inputStream = null;
        try {
            inputStream = Resources.getResourceAsStream(resource);
        } catch (IOException e) {
            // TODO Auto-generated catch block1
            e.printStackTrace();
        }
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        //test_1(sqlSessionFactory);
        //test_2(sqlSessionFactory);
        test_3(sqlSessionFactory);
    }

    private static void test_3(SqlSessionFactory sqlSessionFactory) {
        SqlSession sqlSession = null;
        try {
            sqlSession = sqlSessionFactory.openSession();
            //PageUtil.pageStart(new Page(1, 10));
            RoleMapper roleMapper = sqlSession.getMapper(RoleMapper.class);
            Map<String, String> idsMap = new HashMap<>();
            idsMap.put("key_1", "1");
            idsMap.put("key_2", "2");
            idsMap.put("key_3", "3");
            roleMapper.updateRole(new Role(1L,null,null));
//            List<Role> roleList = roleMapper.getRoleListByMap(idsMap);
//            for(Role role : roleList){
//                //System.out.println(role.getId() + ":" + role.getRoleName() + ":" + role.getNote());
//                System.out.println(role.getUser());
//            }
            //roleList.get(0).getUser();
            sqlSession.commit();
            sqlSession.close();
            System.out.println("------------------执行1----------------------");
//            sqlSession = sqlSessionFactory.openSession();
//            roleMapper = sqlSession.getMapper(RoleMapper.class);
//            roleList = roleMapper.getRoleListByMap(idsMap);
//            sqlSession.commit();
//            sqlSession.close();
//            System.out.println("------------------执行2----------------------");


        } catch (Exception e) {
            // TODO Auto-generated catch block
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    private static void test_2(SqlSessionFactory sqlSessionFactory) {
        SqlSession sqlSession = null;
        try {
            sqlSession = sqlSessionFactory.openSession();
            PageUtil.pageStart(new Page(1, 10));
            RoleMapper roleMapper = sqlSession.getMapper(RoleMapper.class);
            List<String> ids = new ArrayList<>();
            Collections.addAll(ids, "1", "2", "3");
            Role role = roleMapper.getRoleList(ids).get(0);
            System.out.println(role.getId() + ":" + role.getRoleName() + ":" + role.getNote());
            sqlSession.commit();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    private static void test_1(SqlSessionFactory sqlSessionFactory) {
        SqlSession sqlSession = null;
        try {
            sqlSession = sqlSessionFactory.openSession();
            PageUtil.pageStart(new Page(1, 10));
            RoleMapper roleMapper = sqlSession.getMapper(RoleMapper.class);
            Role role = roleMapper.getRole(1L, 2L);
            System.out.println(role.getId() + ":" + role.getRoleName() + ":" + role.getNote());
            sqlSession.commit();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            sqlSession.rollback();
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
    }

    public static void socketTest() throws Exception {
        ServerSocket serverSocket = new ServerSocket(4000);
        serverSocket.accept();
        InputStream is = new FileInputStream("C:\\Users\\Administrator\\Desktop");
    }
}
